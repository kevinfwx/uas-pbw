const container = document.querySelector(".container");
const seattables = document.querySelectorAll(".row .seattable:not(.booked)");
const count = document.getElementById("count");
const total = document.getElementById("total");
const seattableSelect = document.getElementById("seattable");

populateUI();

let ticketPrice = +seattableSelect.value;

// Save selected table index and price
function setSeattableData(seattableIndex, seattablePrice) {
  localStorage.setItem("selectedSeattableIndex", seattableIndex);
  localStorage.setItem("selectedSeattablePrice", seattablePrice);
}

// Update total and count
function updateSelectedCount() {
  const selectedtSeattables = document.querySelectorAll(".row .seattable.selected");

  const seattablesIndex = [...selectedSeattables].map((seattable) => [...seattables].indexOf(seattable));

  localStorage.setItem("selectedSeattables", JSON.stringify(seattablesIndex));

  const selectedSeattablesCount = selectedSeattables.length;

  count.innerText = selectedSeattablesCount;
  total.innerText = selectedSeattablesCount * ticketPrice;

  setSeattableData(seattableSelect.selectedIndex, seattableSelect.value);
}


// Get data from localstorage and populate UI
function populateUI() {
  const selectedSeattables = JSON.parse(localStorage.getItem("selectedSeattables"));

  if (selectedSeattables !== null && selectedSeattables.length > 0) {
    seattables.forEach((seattable, index) => {
      if (selectedSeattables.indexOf(index) > -1) {
        console.log(seattable.classList.add("selected"));
      }
    });
  }

  const selectedSeattableIndex = localStorage.getItem("selectedSeattableIndex");

  if (selectedSeattableIndex !== null) {
    seattableSelect.selectedIndex = selectedSeattableIndex;
    console.log(selectedSeattableIndex)
  }
}
console.log(populateUI())
// Table select event
seattableSelect.addEventListener("change", (e) => {
  ticketPrice = +e.target.value;
  setSeattableData(e.target.selectedIndex, e.target.value);
  updateSelectedCount();
});

// Seat click event
container.addEventListener("click", (e) => {
  if (
    e.target.classList.contains("seattable") &&
    !e.target.classList.contains("booked")
  ) {
    e.target.classList.toggle("selected");

    updateSelectedCount();
  }
});

// Initial count and total set
updateSelectedCount();